#include "isogram.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


bool is_isogram(const char phrase[]) {
  if (phrase == NULL) {
    return false;
  }

  int length = strlen(phrase);
  char ch, prevChars[128] = "";

  for (int i = 0; i < length; i++) {
    ch = tolower(phrase[i]);
    if (ch != '-' && ch != ' ') {

      // check if char has already appeared
      if (strchr(prevChars, ch) != NULL) {
        return false;
      }
      strncat(prevChars, &ch, 1);
    }
  }
  return true;
}
