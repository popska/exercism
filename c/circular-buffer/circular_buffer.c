#include "circular_buffer.h"


circular_buffer_t *new_circular_buffer(size_t capacity) {
  static circular_buffer_t buffer;

  buffer.capacity = capacity;
  buffer.head = 0;
  buffer.tail = 0;
  buffer.count = 0;

  buffer.values = (buffer_value_t *) malloc(sizeof(buffer.values) * capacity);

  return &buffer;
}


int read(circular_buffer_t *buffer, buffer_value_t *value) {
  if (buffer->count == 0) {
    errno = ENODATA;
    return EXIT_FAILURE;
  }

  *value = buffer->values[buffer->head];

  buffer->head++;
  if (buffer->head == buffer->capacity) {
    buffer->head = 0;
  }

  buffer->count--;

  return 0;
}


int write(circular_buffer_t *buffer, buffer_value_t value) {
  if (buffer->count == buffer->capacity) {
    errno = ENOBUFS;
    return EXIT_FAILURE;
  }

  buffer->values[buffer->tail] = value;

  buffer->tail++;
  if (buffer->tail == buffer->capacity) {
    buffer->tail = 0;
  }

  buffer->count++;

  return 0;
}


int overwrite(circular_buffer_t *buffer, buffer_value_t value) {
  if (buffer->count < buffer->capacity) {
    return write(buffer, value);
  }

  buffer->values[buffer->tail] = value;

  buffer->tail++;
  buffer->head++;

  if (buffer->tail == buffer->capacity) {
    buffer->tail = 0;
  }

  if (buffer->head == buffer->capacity) {
    buffer->head = 0;
  }

  return 0;
}

void delete_buffer(circular_buffer_t *buffer) {
  free(buffer->values);
  buffer = NULL;
}


void clear_buffer(circular_buffer_t *buffer) {
  buffer->tail = 0;
  buffer->head = 0;
  buffer->count = 0;
}
