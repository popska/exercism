#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>


typedef int buffer_value_t;

typedef struct {
  buffer_value_t *values;
  size_t tail;
  size_t head;
  size_t capacity;
  size_t count;
} circular_buffer_t;

int write(circular_buffer_t *buffer, buffer_value_t value);
int overwrite(circular_buffer_t *buffer, buffer_value_t value);
int read(circular_buffer_t *buffer, buffer_value_t *value);
circular_buffer_t *new_circular_buffer(size_t capacity);
void delete_buffer(circular_buffer_t *buffer);
void clear_buffer(circular_buffer_t *buffer);

#endif
