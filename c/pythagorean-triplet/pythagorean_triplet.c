#include "pythagorean_triplet.h"
#include <stdlib.h>
#include <stdio.h>


triplets_t *triplets_with_sum(int sum) {
  triplets_t *triplets = (triplets_t*)malloc(sizeof(triplets_t));
  triplets->triplets = (triplet_t*)malloc(100 * sizeof(triplet_t));;

  for (int a = 1; a < sum / 2; a++) {
    for (int b = a; b < sum - a; b++) {
      int c = sum - a - b;
      if ((a * a + b * b) == c * c) {
        triplet_t triplet = { a, b, c };
        triplets->triplets[triplets->count] = triplet;
        triplets->count++;
      }
    }
  }

  return triplets;
}


void free_triplets(triplets_t *triplets) {
  free(triplets->triplets);
  free(triplets);
}
