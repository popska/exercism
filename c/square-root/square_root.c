#include "square_root.h"


int square_root(int n) {
  // implemented using a binary search algorithm
  int min = 0, max = n + 1, mid = n / 2;

  int square = mid * mid;
  while (square != n) {
    if (min > max) {
      return -1;
    }

    if (square > n) {
      max = mid;
    } else {
      min = mid;
    }
    mid = (min + max) / 2;
    square = mid * mid;
  }

  return mid;
}
