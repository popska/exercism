#include "hamming.h"
#include <string.h>


int compute(const char *lhs, const char *rhs) {
  // length of strings
  if (strlen(lhs) != strlen(rhs)) {
    return -1;
  }

  int length = strlen(lhs);
  int difference = 0;
  for (int i = 0; i < length; i++) {
    if (lhs[i] != rhs[i]) {
      difference++;
    }
  }

  return difference;
}
