#include "binary_search.h"


int *binary_search(int value, const int *arr, size_t length) {
  int mid, low = 0, high = length - 1;

  while (high >= low) {
    mid = (high + low) / 2;
    if (arr[mid] < value) {
      low = mid + 1;
    } else if (arr[mid] > value) {
      high = mid - 1;
    } else {
      return (int *)&arr[mid];
    }
  }

  return NULL;
}
