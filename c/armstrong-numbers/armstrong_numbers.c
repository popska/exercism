#include "armstrong_numbers.h"
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

bool is_armstrong_number(int num) {
  int i, sum = 0, digits = log10(num) + 1;

  for (i = 1; i <= digits; i++) {
    sum += (int)pow(num % (int)pow(10, i) / (int)pow(10, i - 1), digits);
  }

  if (sum == num) {
    return true;
  }
  return false;
}
