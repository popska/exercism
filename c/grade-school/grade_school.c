#include "grade_school.h"


void init_roster(roster_t *roster) {
  roster->count = 0;
}

bool add_student(roster_t *roster, char *name, int grade) {
  student_t student;
  strcpy(student.name, name);
  student.grade = grade;

  if (roster->count == MAX_STUDENTS) {
    return false;
  }

  for (size_t i = 0; i < roster->count; i++) {
    if (!strcmp(roster->students[i].name, name)) {
      return false;
    }
  }

  int insertLocation = (int)roster->count;

  for (int i = 0; i < (int)roster->count; i++) {
    if (roster->students[i].grade > grade) {
      insertLocation = i;
      break;
    } else if (roster->students[i].grade == grade) {
      if (strcmp(roster->students[i].name, name) > 0) {
        insertLocation = i;
        break;
      }
    }
  }

  for (int i = roster->count - 1; i >= insertLocation; i--) {
    roster->students[i + 1] = roster->students[i];
  }

  roster->students[insertLocation] = student;

  roster->count++;
  return true;
}

roster_t get_grade(roster_t *roster, int grade) {
  roster_t gradeRoster;

  for (size_t i = 0; i < roster->count; i++) {
    if (roster->students[i].grade == grade) {
      add_student(&gradeRoster, roster->students[i].name, roster->students[i].grade);
    }
  }

  return gradeRoster;
}
