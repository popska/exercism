#include "resistor_color.h"


int color_code(resistor_band_t color) {
  return (int)color;
}

resistor_band_t *colors(void) {
  static resistor_band_t bands[10];

  for (resistor_band_t band = BLACK; band <= WHITE; band++) {
    bands[band] = band;
  }

  return bands;
}
