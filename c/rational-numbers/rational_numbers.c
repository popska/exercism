#include "rational_numbers.h"
#include <math.h>
#include <stdlib.h>


rational_t add(rational_t num0, rational_t num1) {
  rational_t sum;
  sum.numerator = num0.numerator * num1.denominator + num1.numerator * num0.denominator;
  sum.denominator = num0.denominator * num1.denominator;
  return reduce(sum);
}

rational_t subtract(rational_t num0, rational_t num1) {
  rational_t difference;
  difference.numerator = num0.numerator * num1.denominator - num1.numerator * num0.denominator;
  difference.denominator = num0.denominator * num1.denominator;
  return reduce(difference);
}

rational_t multiply(rational_t num0, rational_t num1) {
  rational_t product;
  product.numerator = num0.numerator * num1.numerator;
  product.denominator = num0.denominator * num1.denominator;
  return reduce(product);
}

rational_t divide(rational_t num0, rational_t num1) {
  rational_t inverse = { num1.denominator, num1.numerator };
  return multiply(num0, inverse);
}

rational_t absolute(rational_t num) {
  rational_t result = { abs(num.numerator), abs(num.denominator) };
  return result;
}

rational_t exp_rational(rational_t base, int power) {
  rational_t result;
  result.numerator = (int)pow(base.numerator, power);
  result.denominator = (int)pow(base.denominator, power);
  return result;
}

float exp_real(int base, rational_t power) {
  return pow(base, (float)power.numerator / power.denominator);
}

rational_t reduce(rational_t num) {
  if (num.denominator < 0) {
    num.numerator *= -1;
    num.denominator *= -1;
  }

  int n = gcd(num.numerator, num.denominator);

  rational_t reduced;
  reduced.numerator = num.numerator / n;
  reduced.denominator = num.denominator / n;

  return reduced;
}


// a naive approach to getting the greatest common divisor
int gcd(int a, int b) {
  int gcd = 1;
  a = abs(a);
  b = abs(b);
  int max = a > b ? a : b;

  for (int i = 2; i <= max; i++) {
    if (a % i == 0 && b % i == 0) {
      gcd = i;
    }
  }

  return gcd;
}
