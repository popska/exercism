#ifndef RATIONAL_NUMBERS_H
#define RATIONAL_NUMBERS_H

typedef struct {
   int numerator;
   int denominator;
} rational_t;

rational_t add(rational_t num0, rational_t num1);
rational_t subtract(rational_t num0, rational_t num1);
rational_t multiply(rational_t num0, rational_t num1);
rational_t divide(rational_t num0, rational_t num1);
rational_t absolute(rational_t num);
rational_t exp_rational(rational_t base, int power);
float exp_real(int base, rational_t power);
rational_t reduce(rational_t num);
int gcd(int a, int b);

#endif
