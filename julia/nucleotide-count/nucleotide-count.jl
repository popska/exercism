"""
    count_nucleotides(strand)

The count of each nucleotide within `strand` as a dictionary.

Invalid strands raise a `DomainError`.

"""
function count_nucleotides(strand)
    dict = Dict('A'=>0, 'C'=>0, 'G'=>0, 'T'=>0)
    for nucleotide in strand
        try
            dict[nucleotide] += 1
        catch
            throw(DomainError(nucleotide, "nucleotide must be ACGT"))
        end
    end
    return dict
end

