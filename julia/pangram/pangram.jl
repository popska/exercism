"""
    ispangram(input)

Return `true` if `input` contains every alphabetic character (case insensitive).

"""
function ispangram(input)
    input = lowercase(input)
    for chr in 'a':'z'
        chr in input || return false
    end
    return true
end

